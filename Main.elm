module Main exposing (..)

import Html exposing (..)
import Html.Attributes as Attr
import Http
import Http.Progress as HP exposing (Progress(..))
import Json.Decode as Json
import Time exposing (Time)
import Task


type alias Post =
    { id : Int, title : String }


type Msg
    = GetProgress (Progress String)
    | StartTime Time
    | EndTime Time


type alias Model =
    { posts : List Post, progress : Progress String, errorMessage : Maybe String, startTime : Maybe Time, endTime : Maybe Time }


emptyModel : Model
emptyModel =
    { posts = [], progress = HP.None, errorMessage = Nothing, startTime = Nothing, endTime = Nothing }


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        GetProgress (HP.Done res) ->
            ( { model | progress = HP.Done "" }, getEndTime )

        GetProgress (HP.Fail error) ->
            ( { model | progress = HP.Fail error, errorMessage = Just (toString (Debug.log "Err :: " error)) }, Cmd.none )

        GetProgress progress ->
            ( { model | progress = progress }, Cmd.none )

        StartTime time ->
            ( { model | startTime = Just time }, Cmd.none )

        EndTime time ->
            ( { model | endTime = Just time }, Cmd.none )



-- LoadedPosts (Err _) ->
--     ( model, Cmd.none )
-- LoadedPosts (Ok res) ->
--     ( { model | posts = res }, Cmd.none )


view : Model -> Html Msg
view model =
    let
        p =
            progressLoaded model.progress
                |> toString
    in
        div []
            [ progress [ Attr.value p, Attr.max "100" ]
                [ text (p ++ "%")
                ]
            , br [] []
            , text ("Loading :: " ++ p ++ "%")
            , br [] []
            , text ("Error :: " ++ (maybeToString model.errorMessage))
            , br [] []
            , text ("Total Number of posts :: " ++ toString (List.length model.posts))
            , br [] []
            , text ("Start Time :: " ++ (maybeToTimeString model.startTime))
            , br [] []
            , text ("End Time :: " ++ (maybeToTimeString model.endTime))
            , br [] []
            , text ("Difference :: " ++ (toString ((timeDiff model.endTime model.startTime) / 1000)))
            , br [] []
            , text (" Download Rate :: " ++ (toString (1001 / ((timeDiff model.endTime model.startTime) / 1000))) ++ " kB/s")
            ]


timeDiff : Maybe Time -> Maybe Time -> Time
timeDiff endTime startTime =
    maybeToTime startTime
        |> (-) (maybeToTime endTime)


maybeToTime : Maybe Time -> Time
maybeToTime time =
    case time of
        Just time ->
            time

        Nothing ->
            0


maybeToTimeString : Maybe a -> String
maybeToTimeString time =
    case time of
        Just time ->
            toString time

        Nothing ->
            ""


maybeToString : Maybe a -> String
maybeToString message =
    case message of
        Just msg ->
            toString msg

        Nothing ->
            ""


progressLoaded : HP.Progress String -> Int
progressLoaded progress =
    case progress of
        HP.Some { bytes, bytesExpected } ->
            toFloat bytes
                / toFloat bytesExpected
                |> (*) 100
                |> round

        HP.Done _ ->
            100

        _ ->
            0


getStartTime : Cmd Msg
getStartTime =
    Time.now
        |> Task.perform StartTime


getEndTime : Cmd Msg
getEndTime =
    Time.now
        |> Task.perform EndTime


getPosts : Sub Msg
getPosts =
    -- Http.getString "/request.json"
    -- Http.getString "http://127.0.0.1:8080/request.json"
    -- Http.getString "https://s3.ap-south-1.amazonaws.com/s3cmd-test-public/request.json"
    --     |> HP.track "posts" GetProgress
    let
        request =
            Http.request
                { method = "GET"
                , headers =
                    [ Http.header "Access-Control-Allow-Origin" "*"
                    ]
                , url = "https://s3.ap-south-1.amazonaws.com/s3cmd-test-public/request.json"
                , body = Http.emptyBody
                , expect = Http.expectString
                , timeout = Nothing
                , withCredentials = False
                }
    in
        HP.track "posts" GetProgress request



-- |> Http.send LoadedPosts
-- decodeString


decodePost : Json.Decoder Post
decodePost =
    Json.map2 Post
        (Json.field "id" Json.int)
        (Json.field "title" Json.string)


decodePosts : Json.Decoder (List Post)
decodePosts =
    Json.list decodePost


init : ( Model, Cmd Msg )
init =
    ( emptyModel, getStartTime )


subscriptions : Model -> Sub Msg
subscriptions model =
    getPosts


main : Program Never Model Msg
main =
    Html.program { init = init, view = view, update = update, subscriptions = subscriptions }
